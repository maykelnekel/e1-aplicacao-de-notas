import express from "express";
import { v4 as uuidv4 } from "uuid";

const app = express();

app.listen(3000);
app.use(express.json());

//--- DATABASE ---
let usersList = [];

// --- CLASS USER ---
class User {
  id = uuidv4();
  notes = [];
  constructor(name, cpf) {
    this.name = name;
    this.cpf = cpf;
  }
}

class Note {
    id = uuidv4();
    created_at = Date().toLocaleString();
    constructor(title, content) {
        this.title = title;
        this.content = content;
    }
}
// --- MIDDLEWARES ---
const verifyUser = (req, res, next) => {
  const { cpf } = req.params;
  const verify = usersList.find((user) => user.cpf == cpf);
  if (verify === undefined) {
    return res
      .status(404)
      .json({ error: "invalid cpf - user is not registered" });
  }
  next();
};

// --- USERS ROUTES ---
app.get("/users", (req, res) => {
  res.json(usersList);
});

app.post("/users", (req, res) => {
  const data = req.body;
  const newUser = new User(data.name, data.cpf);
  usersList.push(newUser);

  res.status(201).json(newUser);
});

app.patch("/users/:cpf", verifyUser, (req, res) => {
    const { cpf } = req.params;
    const data = req.body;
    const user = usersList.filter((user) => user.cpf === cpf);
    for (let value in data) {
      user[0][value] = data[value];
    }
    res.json({"message": "updated user successfully", user});
  });

app.delete("/users/:cpf", verifyUser, (req, res) => {
  const { cpf } = req.params;

  usersList = usersList.filter((user) => user.cpf !== cpf);

  res.json({"message": "deleted with successfully", usersList});
});


// --- NOTES ROUTES ---
app.post("/users/:cpf/notes", (req, res)=>{
    const {title, content} = req.body
    const { cpf } = req.params

    const newNote = new Note(title, content)

    const user = usersList.filter((user) => user.cpf === cpf);
    user[0].notes.push(newNote)

    res.status(201).json({ message: `${title} was added into ${user[0].name}'s notes` })
})

app.get("/users/:cpf/notes", (req, res)=>{
    const { cpf } = req.params
    const user = usersList.filter((user) => user.cpf === cpf);

    res.json(user[0].notes)
})

app.patch("/users/:cpf/notes/:id", (req, res)=>{
    const { cpf,id } = req.params
    const data = req.body;
    const user = usersList.filter((user) => user.cpf === cpf);
    const userNote = user[0].notes.filter((note) => note.id === id);
    for (let value in data) {
      userNote[0][value] = data[value];
    }
    userNote[0].updated_at = Date().toLocaleString();

    res.json(user[0].notes);
})

app.delete("/users/:cpf/notes/:id", (req, res)=>{
    const { cpf, id } = req.params

    const user = usersList.filter((user) => user.cpf === cpf);
    user[0].notes = user[0].notes.filter((note) => note.id !== id);

    res.json(user[0].notes)
})